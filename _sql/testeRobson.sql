-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26-Nov-2019 às 01:41
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scrum`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionalidade`
--

CREATE TABLE `funcionalidade` (
  `idHistoria` bigint(20) NOT NULL,
  `idFuncionalidade` bigint(20) NOT NULL,
  `funcionalidade` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `funcionalidade`
--

INSERT INTO `funcionalidade` (`idHistoria`, `idFuncionalidade`, `funcionalidade`) VALUES
(1, 1, 'Elaborar a identidade visual'),
(1, 2, 'Criar página institucional'),
(2, 1, 'Criar Loja virtual'),
(2, 2, 'Criar cadastro e área de clientes'),
(3, 1, 'Criar formulários de contatos'),
(3, 2, 'Conectar com serviços externos de comunicação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historia`
--

CREATE TABLE `historia` (
  `idHistoria` bigint(20) NOT NULL,
  `gostaria` varchar(200) DEFAULT NULL,
  `ra` bigint(20) DEFAULT NULL,
  `objetivo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `historia`
--

INSERT INTO `historia` (`idHistoria`, `gostaria`, `ra`, `objetivo`) VALUES
(1, 'Tornar a marca da empresa mais atrativa', 0, 'Melhorar a visão dos clientes sobre a sua empresa'),
(2, 'Vender pela internet', 0, 'Ampliar as vendas dos produtos'),
(3, 'Terem mais eficiência no atendimento', 2, 'Entrar em contato à distância com a empresa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `ra` bigint(20) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `papel` varchar(200) DEFAULT NULL,
  `senha` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`ra`, `nome`, `papel`, `senha`) VALUES
(0, 'Fidel Angel', 'Product Owner', 'c4ca4238a0b923820dcc509a6f75849b'),
(1, 'Admin', 'Admin', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'Usuarios', 'Clientes', 'c4ca4238a0b923820dcc509a6f75849b'),
(1800019, 'Jade Luany', 'Desenvolvedora', 'c4ca4238a0b923820dcc509a6f75849b'),
(1800221, 'Henrique Souza', 'Scrum Master', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE `projeto` (
  `projeto` varchar(200) NOT NULL,
  `cliente` varchar(200) DEFAULT NULL,
  `projectOwner` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`projeto`, `cliente`, `projectOwner`) VALUES
('Loja Virtual para Empresa de Automação Industrial e Eletrônica', 'Fidel Angel Lopez', 'Fidel Angel Lopez');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sprint`
--

CREATE TABLE `sprint` (
  `idSprint` bigint(20) NOT NULL,
  `sprint` varchar(200) DEFAULT NULL,
  `semana` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sprint`
--

INSERT INTO `sprint` (`idSprint`, `sprint`, `semana`) VALUES
(1, 'Sprint 17/09 a 30/09', '0000-00-00 00:00:00'),
(2, 'Sprint 02 - 01/10 a 15/10', '0000-00-00 00:00:00'),
(3, 'Sprint 03 16/10 - 30/10', '0000-00-00 00:00:00'),
(4, '41415', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefa`
--

CREATE TABLE `tarefa` (
  `idHistoria` bigint(20) NOT NULL,
  `idFuncionalidade` bigint(20) NOT NULL,
  `idTarefa` bigint(20) NOT NULL,
  `tarefa` varchar(200) DEFAULT NULL,
  `idSprint` bigint(20) DEFAULT NULL,
  `ra` bigint(20) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  `termino` date DEFAULT NULL,
  `duracao` float DEFAULT NULL,
  `dependencia` varchar(200) DEFAULT NULL,
  `prioridade` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tarefa`
--

INSERT INTO `tarefa` (`idHistoria`, `idFuncionalidade`, `idTarefa`, `tarefa`, `idSprint`, `ra`, `status`, `inicio`, `tempo`, `termino`, `duracao`, `dependencia`, `prioridade`) VALUES
(1, 1, 2, 'Teste', 3, 0, 'A fazer', '0000-00-00', '00:00:00', '0000-00-00', 1, '', 'Baixa'),
(1, 1, 3, 'Deploy', 2, 1800221, 'A fazer', '0000-00-00', '00:00:00', '0000-00-00', 1, '', 'Baixa'),
(1, 1, 11, 'Criar uma paleta de cores', 4, 1800019, 'A fazer', '0000-00-00', '00:00:00', '0000-00-00', 2, '', 'Baixa'),
(1, 2, 3, 'Deploy', NULL, 1800221, 'A fazer', NULL, NULL, NULL, 1, '', 'Baixa'),
(1, 2, 12, 'Desenvolver a pagina', 2, 1800019, 'Feito', '2019-09-18', '00:00:02', '2019-09-20', 6, '', 'Media');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `funcionalidade`
--
ALTER TABLE `funcionalidade`
  ADD PRIMARY KEY (`idHistoria`,`idFuncionalidade`);

--
-- Indexes for table `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`idHistoria`),
  ADD KEY `ra` (`ra`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`ra`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`projeto`);

--
-- Indexes for table `sprint`
--
ALTER TABLE `sprint`
  ADD PRIMARY KEY (`idSprint`);

--
-- Indexes for table `tarefa`
--
ALTER TABLE `tarefa`
  ADD PRIMARY KEY (`idHistoria`,`idFuncionalidade`,`idTarefa`),
  ADD KEY `ra` (`ra`),
  ADD KEY `idSprint` (`idSprint`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `historia`
--
ALTER TABLE `historia`
  MODIFY `idHistoria` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `funcionalidade`
--
ALTER TABLE `funcionalidade`
  ADD CONSTRAINT `funcionalidade_ibfk_1` FOREIGN KEY (`idHistoria`) REFERENCES `historia` (`idHistoria`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `historia`
--
ALTER TABLE `historia`
  ADD CONSTRAINT `historia_ibfk_1` FOREIGN KEY (`ra`) REFERENCES `pessoa` (`ra`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tarefa`
--
ALTER TABLE `tarefa`
  ADD CONSTRAINT `tarefa_ibfk_1` FOREIGN KEY (`idHistoria`,`idFuncionalidade`) REFERENCES `funcionalidade` (`idHistoria`, `idFuncionalidade`),
  ADD CONSTRAINT `tarefa_ibfk_2` FOREIGN KEY (`ra`) REFERENCES `pessoa` (`ra`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `tarefa_ibfk_3` FOREIGN KEY (`idSprint`) REFERENCES `sprint` (`idSprint`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
