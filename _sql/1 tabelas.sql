DROP DATABASE IF EXISTS scrum;
CREATE DATABASE scrum;
USE scrum;

-- cria tabelas
-- projeto

CREATE TABLE Projeto(
					projeto 		varchar (200) not null primary key,
					cliente 		varchar (200),
					projectOwner 	varchar (200)
);

-- usuários

CREATE TABLE Pessoa(
					ra 		bigint (20) not null primary key,
					nome 	varchar (200),
					papel 	varchar (200),
					senha 	varchar (200) 
);

--
CREATE TABLE Sprint (
					keysprint   bigint (20) not null auto_increment primary key,
					idSprint 	bigint (20),
					sprint 		varchar (200),
					semana 		timestamp 
);

--
CREATE TABLE Historia (
						idHistoria 	bigint (20) not null auto_increment primary key,
						gostaria 	varchar (200),
						ra 			bigint (20),
						objetivo 	varchar (200),
	foreign key (ra) 	references Pessoa(ra) ON UPDATE CASCADE ON DELETE SET NULL
);

--
CREATE TABLE Funcionalidade (
							idHistoria 			bigint(20) not null,
							idFuncionalidade 	bigint(20) not null,

							funcionalidade 		varchar(191),
	primary key (idHistoria, idFuncionalidade),
	foreign key (idHistoria) 	references Historia(idHistoria) ON DELETE CASCADE
);

CREATE TABLE Tarefa (
					idHistoria 			bigint(20) not null,
					idFuncionalidade 	bigint(20) not null,
					idTarefa 			bigint(20) not null,
					keysprint           bigint (20),
					tarefa 				varchar(200),
					idSprint 			bigint(20),
					ra 					bigint (20),
					status 				varchar(200),						
					inicio 				date,
					tempo 				time,
					termino 			date,
					duracao 			float,
					dependencia 		varchar(200),
					prioridade 			varchar(200),
	primary key (idHistoria, idFuncionalidade, idTarefa),
	foreign key (idHistoria, idFuncionalidade) 	references Funcionalidade(idHistoria, idFuncionalidade),
	foreign key (ra) 							references Pessoa(ra) ON UPDATE CASCADE ON DELETE SET NULL,
	foreign key (keysprint) 						references Sprint(keysprint));


select * from tarefa

