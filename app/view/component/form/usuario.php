
<?php
$configs = include(__DIR__ . '/../../../../config.php');
?>
<form action="<?=$configs['document_root']?>/alteraSenha/<?= Session::getSessionRa() ?>" method="post">
    <div class="form-row">
        <div class="form-group col-md-2">
            <label for="inputNome">Nome: </label>
            <input type="text" class="form-control" id="inputNome" aria-describedby="inputNome"
                   placeholder="nome" name="nome" value="<?=$usuario['nome']?>" readonly>
        </div>
        <div class="form-group col-md-3">
            <label for="inputRA">RA: </label>
            <input type="number" class="form-control" id="inputRA" aria-describedby="inputRA"
                   placeholder="ra" name="ra" value="<?= Session::getSessionRa() ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label for="inputSenha">Senha: </label>
        <input type="password" class="form-control" id="inputSenha" aria-describedby="inputSenha"
               placeholder="Digite sua nova senha" name="Senha" value="">
    </div>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>