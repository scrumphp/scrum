

<?php
$configs = include(__DIR__ . '/../../../../config.php');
?>

<table class="table">  
    <thead class="thead-light">
    <tr>
        <th scope="col">Papel</th>
        <th scope="col">Nome</th>
        <th scope="col">RA</th>
        <th scope="col" colspan="2" class="d-print-none"><a class="btn btn-danger" href="<?=$configs['document_root']?>/pessoa/insere/"?>Adicionar Pessoa <i class="fas fa-plus"></i></a></th>
        </th>
    </tr>
    </thead>
    <body>
    <?php        
    foreach ($pessoas as $pessoa) :
        ?>
        <tr>
            <td><?= $pessoa['papel'] ?></td>
            <td><?= $pessoa['nome'] ?></td>
            <td><?= $pessoa['ra'] ?></td>
            
<td  class="d-print-none"><a class="d-print-none"><a class="btn btn-primary" href="<?=$configs['document_root']?>/pessoa/editar/<?= $pessoa['ra']?>"><i class="fas fa-edit"></i></a>
    </a></td>
            
<td class="d-print-none"><a class="btn btn-danger" href="<?=$configs['document_root']?>/pessoa/remove/<?= $pessoa['ra']?>"><i class="fas fa-trash"></i></a></td>
        
        
        </tr>
        <?php
    endforeach
    ?>

    </body>
</table>
<button value="Voltar" class="btn btn-primary" onclick="window.history.back();">Voltar</button>

<button type="submit" class="btn btn-primary" value="Imprimir" onClick="window.print()">Imprimir</button>

