<?php
/**
 
 */

require_once(__DIR__ . "/../../template/header.php");

require_once(__DIR__ . "/../../../security/session/Session.php");
Session::redirectIfNotLogged();

?>

<div class="row ml-0"  >

<?php

require_once(__DIR__ . "/../../component/table/cabecalho.php");
?>

<div class="col-md-4 float-right">
    <ul class="list-group">
        <li class="list-group-item"> edite o início/termino e o tempo</li >
    </ul >
</div >

<?php
require_once(__DIR__ . "/../../component/table/cronograma.php");


require_once(__DIR__ . "/../../template/footer.php");