<?php


require_once(__DIR__ . "/../../template/header.php");

require_once(__DIR__ . "/../../../security/session/Session.php");
Session::redirectIfNotLogged();

if($post != null) {
    if($updated) { ?>
        <p class="alert alert-success d-print-none" role="alert">Tarefa adicionada com sucesso!</p>
        <div class="row ml-0"  >
            <?php
            require_once(__DIR__ . "/../../component/table/cabecalho.php");
            ?>
            <div class="col-md-4 float-right">
    <ul class="list-group list-group-flush">
        <li class="list-group-item"> h = História </li >
        <li class="list-group-item"> d = Duração </li >
        <!-- <li class="list-group-item"> q / s = quantidade de sprints </li >
        <li class="list-group-item"> t / h = tempo de história </li > -->
    </ul >
</div >
        </div >
        <?php
        require_once(__DIR__ . "/../../component/table/product-backlog.php");
     } else {
        $msg = mysqli_error($conexao);
        ?>
        <p class="alert alert-danger" role="alert">Tarefa não foi adicionada: <?= $msg ?></p>
        <?php
        require_once(__DIR__ . "/../../component/form/product-backlogAdd.php");
    }
}else{
    require_once(__DIR__ . "/../../component/form/product-backlogAdd.php");
}



require_once(__DIR__ . "/../../template/footer.php");